package com.example.storekeeper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Objects;

import util.ShopKeeperApi;

public class LoginActivity extends AppCompatActivity {

    private Button loginBtn;
    private Button createAcctBtn;


    //login fields
    private AutoCompleteTextView emailAddress;
    private EditText password;
    private ProgressBar progressBar;

    //firebase
    private FirebaseAuth firebaseAuth;
    private  FirebaseAuth.AuthStateListener authStateListener; //listens for auth changes
    private FirebaseUser currentUser; //helps access current user

    //firestore connection
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    //create collection to store users
    private CollectionReference collectionReference = db.collection("Users");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //remove border from toolbar
        Objects.requireNonNull(getSupportActionBar()).setElevation(0);

        progressBar = findViewById(R.id.login_progress_bar);

        firebaseAuth = FirebaseAuth.getInstance();

        loginBtn = findViewById(R.id.login_btn);
        createAcctBtn = findViewById(R.id.create_acct_btn);

        emailAddress = findViewById(R.id.txt_email);
        password = findViewById(R.id.txt_password);


        //open signupactivity  if user click on create account
        createAcctBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
            }
        });

        //login user
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginUser(emailAddress.getText().toString().trim(),
                        password.getText().toString().trim());
            }
        });
    }

    //outside oncreate

    private void loginUser(String email, String password){

        progressBar.setVisibility(View.VISIBLE);

        if(!TextUtils.isEmpty(email) &&
        !TextUtils.isEmpty(password)){

            //signin user
            firebaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            //at this point, it has matched the user with the credentials
                            //next, we proceed to fetch user details from Users collection
                            //get the user
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            assert user != null;
                            String currentUserId = user.getUid();

                            //loop through Users collection
                            collectionReference.whereEqualTo("userId", currentUserId)
                                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                        @Override
                                        public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                                            //at this point, we retrieve the data

                                            if(error != null){

                                            }

                                            assert value != null;
                                            if(!value.isEmpty()){

                                                progressBar.setVisibility(View.INVISIBLE);

                                                //use singleton class to hold values that we'll pass around

                                                //loop through the collection and get the username and userid
                                                //the collection will mostly contain just one item
                                                //but its good to loop through
                                                //the field names 'username and userId' must be same as what you
                                                //have in your firebase collection
                                                for(QueryDocumentSnapshot snapshot : value){

                                                    ShopKeeperApi shopKeeperApi = ShopKeeperApi.getInstance();
                                                    //
                                                    shopKeeperApi.setUsername(snapshot.getString("username"));
                                                    shopKeeperApi.setUserId(snapshot.getString("userId"));

                                                    //redirect to addinventory activity
                                                    startActivity(new Intent(LoginActivity.this, AddInventoryActivity.class));
                                                }
                                            }
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });

        }else{
            progressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(LoginActivity.this,
                    "Email and password are require",
                    Toast.LENGTH_LONG)
                    .show();
        }
    }
}