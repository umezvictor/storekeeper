package com.example.storekeeper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import util.ShopKeeperApi;

public class SignupActivity extends AppCompatActivity {

    //init fields
    private Button createAccountBtn;
    private Button loginBtn;
    private EditText emailEditText;
    private EditText passwordEditText;
    private ProgressBar progressBar;
    private EditText usernameEditText;

    //firebase
    private FirebaseAuth firebaseAuth;
    private  FirebaseAuth.AuthStateListener authStateListener; //listens for auth changes
    private FirebaseUser currentUser; //helps access current user

    //firestore connection
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    //create collection to store users
    private CollectionReference collectionReference = db.collection("Users");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        firebaseAuth = FirebaseAuth.getInstance();

        createAccountBtn = findViewById(R.id.register_btn);
        progressBar = findViewById(R.id.signup_progress_bar);
        usernameEditText = findViewById(R.id.username);
        emailEditText = findViewById(R.id.txt_signup_email);
        passwordEditText = findViewById(R.id.txt_signup_password);

        //listen to changes in authentication state
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //listens to state changes
                currentUser = firebaseAuth.getCurrentUser();

                if(currentUser != null){
                    //user exists
                }else{
                    //no user
                }
            }
        };

        //create user account -- when user clicks on create account button
        createAccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //validate user input
                if(!TextUtils.isEmpty(emailEditText.getText().toString()) &&
                        !TextUtils.isEmpty(passwordEditText.getText().toString()) &&
                        !TextUtils.isEmpty(usernameEditText.getText().toString())){

                    String email = emailEditText.getText().toString().trim();
                    String password = passwordEditText.getText().toString().trim();
                    String username = usernameEditText.getText().toString().trim();

                    createUserEmailAccount(email, password, username);
                }else{
                    Toast.makeText(SignupActivity.this, "Empty fields are not allowed",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void createUserEmailAccount(String email, String password, final String username){
        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(username)){
            //show progress bar
            progressBar.setVisibility(View.VISIBLE);

            //create account
            firebaseAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            //if success -- 'task' object will hold the user created
                            //at this point user has been created
                            //everything else below is extra
                            //just trying to create a user collection in firestore
                            if(task.isSuccessful()){
                                //successful
                                //add user to our users collection in firestore so we can track them
                                currentUser = firebaseAuth.getCurrentUser();
                                assert currentUser != null;
                                //get user id
                                final String currentUserId = currentUser.getUid();
                                //create user map so we can create a user in collection
                                Map<String, String> userObject = new HashMap<>();
                                userObject.put("userId", currentUserId);
                                userObject.put("username", username);

                                //save userObject to firestore collection
                                collectionReference.add(userObject)
                                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                            @Override
                                            public void onSuccess(DocumentReference documentReference) {
                                                //document ref holds the data we just saved
                                                documentReference.get()
                                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                                //add this point everything is complete
                                                                if(Objects.requireNonNull(task.getResult()).exists()){
                                                                    progressBar.setVisibility(View.INVISIBLE);

                                                                    //GET USERNAME
                                                                    String name = task.getResult()
                                                                            .getString("username");

                                                                    //use the shopkeeperapi -- global singleton class to hold the user data passed to the intent below
                                                                    //we'll use it to retrieve the data in the next activity
                                                                    ShopKeeperApi shopKeeperApi = ShopKeeperApi.getInstance();
                                                                    shopKeeperApi.setUsername(name);
                                                                    shopKeeperApi.setUserId(currentUserId);

                                                                    //launch addInventoryActivity after user signs up
                                                                    Intent intent = new Intent(SignupActivity.this,
                                                                            AddInventoryActivity.class);
                                                                    intent.putExtra("username", name);
                                                                    intent.putExtra("userId", currentUserId);
                                                                    startActivity(intent);
                                                                }else{
                                                                    //if document snapshot is null
                                                                    progressBar.setVisibility(View.INVISIBLE);
                                                                }
                                                            }
                                                        });
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {

                                            }
                                        });
                            }else{
                                //something went wrong

                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //if it fails
                        }
                    });
        }else{

        }
    }

    //get current user before anything else starts once activity
    @Override
    protected void onStart() {
        super.onStart();
        currentUser = firebaseAuth.getCurrentUser();
        firebaseAuth.addAuthStateListener(authStateListener); //listens for changes
    }
}