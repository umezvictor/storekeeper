package com.example.storekeeper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Objects;

import util.ShopKeeperApi;

public class MainActivity extends AppCompatActivity {

    private Button getStartedButton;


    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseUser currentUser;

    //firestore
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    //user info is saved in "Users" collection in firestore
    private CollectionReference collectionReference = db.collection("Users");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getStartedButton = findViewById(R.id.start_btn);

        //remove border from toolbar
        //paste this in other activities with toolbar
        Objects.requireNonNull(getSupportActionBar()).setElevation(0);

        //get auth details
        //if user session exists, redirect to inventory list and skip login activity
        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //get current user
                currentUser = firebaseAuth.getCurrentUser();
                if(currentUser != null){
                    currentUser = firebaseAuth.getCurrentUser();
                    //get user id
                    String currentUserId = currentUser.getUid();
                    //find user in collection
                    collectionReference
                            .whereEqualTo("userId", currentUserId)
                            .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                                    //check if error occured
                                    if(error != null){
                                        return;
                                    }

                                    String name;

                                    //loop through QuerySnapshot --value
                                    if(!value.isEmpty()){
                                        for(QueryDocumentSnapshot snapshot : value){

                                            //invoke my singleton class to hold user data
                                            ShopKeeperApi shopKeeperApi = ShopKeeperApi.getInstance();
                                            //get userid from userId field
                                            //can also get from currentUserId
                                            shopKeeperApi.setUserId(snapshot.getString("userId"));
                                            shopKeeperApi.setUsername(snapshot.getString("username"));

                                            //redirect user to inventory list activity
                                            startActivity(new Intent(MainActivity.this,
                                                    InventoryListActivity.class));

                                            finish();//prevents user from coming back to main activity

                                        }
                                    }
                                }
                            });
                }else{

                }
            }
        };


        getStartedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //go to login activity
                startActivity(new Intent(MainActivity.this,
                        com.example.storekeeper.LoginActivity.class));
                finish();//gets rid of this activity as user moves forward
            }
        });
    }

    //when authstatelistener is used, ensure to cleanup everything after
    //leaving actvity

    @Override
    protected void onStart() {
        super.onStart();
        //get current user
        currentUser = firebaseAuth.getCurrentUser();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(firebaseAuth != null){
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }
}