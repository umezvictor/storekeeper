package com.example.storekeeper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Date;
import java.util.Objects;

import model.Inventory;
import util.ShopKeeperApi;

public class AddInventoryActivity extends AppCompatActivity implements View.OnClickListener {

    //init input fields variable
    private EditText itemNameET;
    private EditText itemPriceET;
    private EditText itemQuantityET;
    private Button saveItemButton;
    private ProgressBar saveItemProgressBar;
    private ImageView addPhotoButton;
    private TextView currentUserTextView;
    private ImageView backgroundImageView;

    //user info
    private String currentUserId;
    private String currentUsername;
    public static final int GALLERY_CODE = 1;
    public static final String TAG = "AddInventoryActivity";

    //firebase instance variables
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseUser user;

    //connection to firestore
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    //when user uploads an image it saves to storage in firebase
    //we need a reference to storage
    private StorageReference storageReference;
    //create a collection reference
    private CollectionReference collectionReference = db.collection("inventory");
    private Uri imageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_inventory);

        //remove border from toolbar
        Objects.requireNonNull(getSupportActionBar()).setElevation(0);


        storageReference = FirebaseStorage.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        saveItemProgressBar = findViewById(R.id.saveItemProgressBar);
        itemNameET = findViewById(R.id.txtItemName);
        itemPriceET = findViewById(R.id.txtPrice);
        itemQuantityET = findViewById(R.id.txtQuantity);
        saveItemButton = findViewById(R.id.saveBtn);
        saveItemButton.setOnClickListener(this);
        addPhotoButton = findViewById(R.id.cameraIcon);
        addPhotoButton.setOnClickListener(this);
        currentUserTextView = findViewById(R.id.usernameTextView);
        backgroundImageView = findViewById(R.id.itemImageView);

        //hide progress bar when save button is clicked
        saveItemProgressBar.setVisibility(View.INVISIBLE);

        //fetch user info from ShopkeeperApi singleton class
        //remember, we've used it to hold user info after login
        //it makes the user data available throughout the app
        //nee need to go fetch user data from db

        if(ShopKeeperApi.getInstance() != null){
            currentUserId = ShopKeeperApi.getInstance().getUserId();
            currentUsername = ShopKeeperApi.getInstance().getUsername();

            currentUserTextView.setText(currentUsername);
        }

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if(user != null){

                }else{

                }
            }
        };

    }

    //check if save button or upload button was clicked
    @Override
    public void onClick(View view) {
        //handles both buttons
        switch(view.getId()){
            case R.id.saveBtn:
                //save item
                saveItem();
                break;
            case R.id.cameraIcon:
                //upload photo
                //when you click the upload button, it starts a new activity
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                //get any image related file
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_CODE);
                break;
        }
    }

    private void saveItem() {
        //save item

        //get user input
        final String itemName = itemNameET.getText().toString().trim();
        final String itemPrice = itemPriceET.getText().toString().trim();
        final String itemQuantity = itemQuantityET.getText().toString().trim();

        //show progressbar
        saveItemProgressBar.setVisibility(View.VISIBLE);

        //validate input
        if(!TextUtils.isEmpty(itemName) && !TextUtils.isEmpty(itemPrice)
                && !TextUtils.isEmpty(itemQuantity) && imageUri != null){

           //create a filepath in the storage reference to save uploaded image
            final StorageReference filePath = storageReference
                    .child("store_images")
                    .child("my_image_" + Timestamp.now().getSeconds());//unique filename for each image

            //save the image
            filePath.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                            //retrieve the imageurl after image has been uploaded
                            //helps in retrieving the image later on

                            filePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    //create an inventory object
                                    Inventory inventory = new Inventory();
                                    inventory.setItemName(itemName);
                                    inventory.setItemPrice(itemPrice);
                                    inventory.setItemQuantity(itemQuantity);
                                    inventory.setImageUrl(uri.toString());
                                    inventory.setCreatedAt(new Timestamp(new Date()));
                                    inventory.setUserName(currentUsername);
                                    inventory.setUserId(currentUserId);

                                    //invoke a collection reference
                                    collectionReference.add(inventory)
                                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                                @Override
                                                public void onSuccess(DocumentReference documentReference) {
                                                    //if success
                                                    saveItemProgressBar.setVisibility(View.INVISIBLE);
                                                    //invoke the inventorylist activity
                                                    startActivity(new Intent(AddInventoryActivity.this,
                                                            InventoryListActivity.class));
                                                    //get rid of  activity
                                                    finish();
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    //log error
                                                    Log.d(TAG, "ErrorSavingItem: " + e.getMessage());
                                                }
                                            });

                                    //save inventory instance
                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //it it fails
                            saveItemProgressBar.setVisibility(View.INVISIBLE);
                        }
                    });


        }else{
            saveItemProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    //after the picture has been uploaded, handle the data
    //onActivityResult -- override this result

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //if photo uploaded successfully
        if(requestCode == GALLERY_CODE && resultCode == RESULT_OK){
            //check if the data is null
            if(data != null){
                //get the image path
                //use it as background image for imageView
                imageUri = data.getData();
                backgroundImageView.setImageURI(imageUri);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        user = firebaseAuth.getCurrentUser();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //stop the listener after app stops
        if(firebaseAuth != null){
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }
}