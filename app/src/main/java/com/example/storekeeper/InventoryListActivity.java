package com.example.storekeeper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import model.Inventory;
import ui.InventoryRecyclerViewAdapter;
import util.ShopKeeperApi;

public class InventoryListActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseUser user;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private StorageReference storageReference;

    private List<Inventory> inventoryList;
    private RecyclerView recyclerView;
    private InventoryRecyclerViewAdapter recyclerViewAdapter;

    //init fb collection
    private CollectionReference collectionReference =
            db.collection("inventory");//inventory is the name of my fb collection

    private TextView noInventoryItem;//text  view that shows if no item is available

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_list);

        //remove border from toolbar
        Objects.requireNonNull(getSupportActionBar()).setElevation(0);

        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();

        noInventoryItem = findViewById(R.id.textViewNoInventoryItem);

        inventoryList = new ArrayList<>();

        //get the recyclerView in activity_inventory_list -- the default activity for this class
        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }


    //invoked when the menu bar is clicked
    //this is where you use a layout resource file
    //it's different from an activity
    //its not a new page(activity)
    //it's something you display within an activity


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //prepare the menu
        //pass in the menu file created
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //invoked when a menu item is selected
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            //redirect user based on menu item selected
            case R.id.action_add:
                //take users to add inventory activity, if user clicks on add icon
                if(user != null && firebaseAuth != null){
                    startActivity(new Intent(InventoryListActivity.this,
                            AddInventoryActivity.class));
                    finish();
                }
                break;
            case R.id.sign_out_menu_icon:
                //logout user, go back to main activity - home
                if(user != null && firebaseAuth != null){
                    firebaseAuth.signOut();

                    startActivity(new Intent(InventoryListActivity.this,
                            MainActivity.class));
                    finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //onstart

    @Override
    protected void onStart() {
        //at this point, I retrieve all inventory items from firestore
        super.onStart();

        //query the db collection at this point
//get user's inventory collection
        collectionReference.whereEqualTo("userId",
                ShopKeeperApi.getInstance().getUserId())
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if(!queryDocumentSnapshots.isEmpty()){
                            for(QueryDocumentSnapshot inventoryItems : queryDocumentSnapshots){
                                //pass items into inventoryList and feed it into the recyclerview

                                //cast inventoryItems to type Inventory
                                Inventory inventory = inventoryItems.toObject(Inventory.class);
                                //add the item to the list
                                inventoryList.add(inventory);
                            }

                            //invoke recyclerView
                            recyclerViewAdapter = new InventoryRecyclerViewAdapter(InventoryListActivity.this, inventoryList);
                            //pass in the adapter to the recyclerView
                            recyclerView.setAdapter(recyclerViewAdapter);
                            recyclerViewAdapter.notifyDataSetChanged();

                        }else{
                                noInventoryItem.setVisibility(View.VISIBLE);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });

    }
}