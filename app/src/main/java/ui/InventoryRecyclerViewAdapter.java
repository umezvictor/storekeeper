package ui;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.storekeeper.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import model.Inventory;

public class InventoryRecyclerViewAdapter extends RecyclerView.Adapter<InventoryRecyclerViewAdapter.MyViewHolder> {

    private Context context;
    private List<Inventory> inventoryList;


    public InventoryRecyclerViewAdapter(Context context, List<Inventory> inventoryList) {
        this.context = context;
        this.inventoryList = inventoryList;
    }

    @NonNull
    @Override
    public InventoryRecyclerViewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //this view is created  by invoking the inventoryrecords layout file
        View view = LayoutInflater.from(context)
                .inflate(R.layout.inventory_records, parent, false);

        return new MyViewHolder(view, context);
        //next go to onbind and bind the data that is retrieved from db
    }

    //bind data here
    @Override
    public void onBindViewHolder(@NonNull InventoryRecyclerViewAdapter.MyViewHolder viewHolder, int position) {
        Inventory inventory = inventoryList.get(position);//points to a single item in the list
        String imageUrl;

        //bind data to the vh fields
        viewHolder.itemName.setText(inventory.getItemName());
        viewHolder.price.setText(inventory.getItemPrice());
        viewHolder.quantity.setText(inventory.getItemQuantity());
        viewHolder.name.setText(inventory.getUserName());

        //use picasso library (from squareup)to download and show images
        imageUrl = inventory.getImageUrl();

        //display time in a span -eg "2 dyas ago, 1 hour ago" format
        String timeAgo = (String) DateUtils.getRelativeTimeSpanString(inventory.getCreatedAt().getSeconds() * 1000); //to get eg 1 hour ago, multiply by 1000

        viewHolder.dateCreated.setText(timeAgo);
//.placeholder(R.drawable.shopkeeper) displays a default image/placeholder if no image is downloaded
        Picasso.get()
                .load(imageUrl)
                .placeholder(R.drawable.shopkeeper)
                .fit()
                .into(viewHolder.itemImage);
    }

    @Override
    public int getItemCount() {
        return inventoryList.size();
    }

    //this class will hold all the fields/items in the inventory records
    //layout file that will display the items from the db
    //this class will be invoked in the onCreateViewHolder class
    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView itemName, price, quantity, dateCreated, name;
        public ImageView itemImage;
        public ImageButton shareButton;

        String userId;
        String userName;

        //context is useful for navigating to a new activity
        public MyViewHolder(@NonNull View itemView, Context ctx) {
            super(itemView);
            context = ctx;
            //map fields
            itemName = itemView.findViewById(R.id.item_name_list);
            price = itemView.findViewById(R.id.item_price_list);
            quantity = itemView.findViewById(R.id.item_quantity_list);
            dateCreated = itemView.findViewById(R.id.item_timestamp);
            itemImage = itemView.findViewById(R.id.item_image_list);
            name = itemView.findViewById(R.id.username_display_in_records);
            shareButton = itemView.findViewById(R.id.share_button);

            shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }
}
