package model;


import com.google.firebase.Timestamp;

public class Inventory {
    //model class for inventory
    private String itemName;
    private String itemPrice;
    private String itemQuantity;

    private String userId;
    private String userName;
    private Timestamp createdAt;
    private String imageUrl;


    //empty constructor -- necessary for firestore
    public Inventory(){}

    //constructor 2
    public Inventory(String itemName, String itemPrice, String itemQuantity, String userId, String userName, Timestamp createdAt, String imageUrl) {
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.itemQuantity = itemQuantity;
        this.userId = userId;
        this.userName = userName;
        this.createdAt = createdAt;
        this.imageUrl = imageUrl;
    }

    //getters and setters

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(String itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
