package util;

import android.app.Application;

//this is a global class that is available throughout the application
//because it extends Application
//note -- go to manifest file and configure it as well
//we'll use it to hold user data passed to next activity after signup
public class ShopKeeperApi extends Application {
    private String username;
    private String userId;


    //we make it a singleton by creating the property below
    //the idea is that you don't need to instantiate this class
    private static ShopKeeperApi instance;

    public static ShopKeeperApi getInstance(){
        if(instance == null){
            instance = new ShopKeeperApi();
        }
        return instance;
    }
    public ShopKeeperApi(){}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
